const mongoose = require("mongoose");
const productInCartScheme = require("../schemes/productInCartScheme");

const ProductInCart = mongoose.model("ProductInCart", productInCartScheme);

ProductInCart.addNew = async (values)=>{
  const alreadyIn = await ProductInCart.findOne({name:values.name, userId:values.userId});
  if(alreadyIn){
    alreadyIn.sum = Number(alreadyIn.sum) + Number(values.sum);
    alreadyIn.amount = Number(alreadyIn.amount) + Number(values.amount);
    const updated = await alreadyIn.save();
    return updated;
  }
  delete values._id;
  const productInCart = new ProductInCart(values);
  productInCart.id = productInCart._id;
  let result = await productInCart.save();
  return result;
};

ProductInCart.countAmount = async (id)=>{
  let result = await ProductInCart.find({userId:id});
  let total = 0;
  for(let i = 0; i < result.length; i++){
    total += result[i].amount;
  }
  return total;
};

ProductInCart.getAll = async (id)=>{
  let result = await ProductInCart.find({userId:id});
  return result;
};

ProductInCart.removeOne = async (id)=>{
  let result = await ProductInCart.deleteOne({id:id});
  return result;
};

ProductInCart.removeAll = async (id)=>{
  let result = await ProductInCart.deleteMany({userId:id});
  return result;
};

module.exports = ProductInCart;
