const mongoose = require("mongoose");
const preOrderSchema = require("../schemes/preOrderScheme");

const PreOrder = mongoose.model("preOrder", preOrderSchema);

PreOrder.addNew = async (values)=>{
  const preOrder = new PreOrder(values);
  let result = preOrder.save();
  return result;
};

PreOrder.getLastOne = async (id)=>{
  let lastOrder = await PreOrder.findOne({userId:id}, {}, {sort:{"orderDate":-1}});
  return (lastOrder);
};

PreOrder.removeAll = async (id)=>{
  let result = await PreOrder.deleteMany({userId:id});
  return result;
};

module.exports = PreOrder;
