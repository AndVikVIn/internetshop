const mongoose = require("mongoose");
const manufacterScheme = require("../schemes/manufacterSchema");

const Manufacter = mongoose.model("Manufacter", manufacterScheme);

module.exports = Manufacter;
