const mongoose = require("mongoose");
const orderScheme = require("../schemes/orderScheme");

const Order = mongoose.model("Order", orderScheme);

Order.addNew = async (values)=>{
  values.name = values.name.split(",");
  values.amount = values.amount.split(",");
  let newOrder = new Order(values);
  let result = newOrder.save();
  return result;
};


Order.changeOne = async (item)=>{
  const order = await Order.findOne({_id:item._id});
  order.status = item.status;
  if(item.status == "Declined"){
    order.declineReason = item.reason;
  } else {
    order.declineReason = "";
  }
  let result = await order.save();
  return  result;
};

Order.getAll = async ()=>{
  let result = await Order.find({});
  return result;
};

Order.getUserOrders = async (id)=>{
  let result = await Order.find({userId:id});
  return result;
};

module.exports = Order;
