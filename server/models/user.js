const mongoose = require("mongoose");
const userScheme = require("../schemes/userSchema");
const bcrypt = require("bcrypt");
const saltRounds = 10;

const User = mongoose.model("User", userScheme);

User.createNew = async (obj)=>{
  const hashPass = await bcrypt.hash(obj.password, saltRounds);
  obj.password = hashPass;
  const newUser = new User(obj);
  newUser.id = newUser._id;
  let result = await newUser.save();
  return result;
};

User.getAll = async ()=>{
  let result = await User.find({});
  return result;
};

User.updateUser = async (id, property, value)=>{
  const user = await User.findOne({_id:id});
  user[property] = value;
  let result = await user.save();
  return result;
};

User.checkEmail = async (obj)=>{
  const result = await User.findOne({email:obj});
  return result;
};




module.exports = User;
