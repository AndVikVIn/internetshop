const mongoose = require("mongoose");
const productTypeSchema = require("../schemes/productTypeSchema");

const Type = mongoose.model("Type", productTypeSchema);

Type.showTree = async ()=>{
 return await Type.aggregate([
    {
      $lookup:{
        from:"products",
        localField:"_id",
        foreignField:"type",
        as:"data"
      }
    },
    {$project:{"value":1, "data.manufacter":1}},
    {
      $lookup:{
        from:"manufacters",
        localField:"data.manufacter",
        foreignField:"_id",
        as:"data"
      }
    },
  ]);
};

module.exports = Type;
