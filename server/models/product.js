const mongoose = require("mongoose");
const productSchema = require("../schemes/productScheme");

const Product = mongoose.model("Product", productSchema);
Product.getAll = async ()=>{
  let result = await Product.find({});
  return result;
};

Product.createNew = async (obj)=>{
  const newProduct = new Product(obj);
  newProduct.id = newProduct._id;
  let result = await newProduct.save();
  return result;
};

Product.increaseRating = async (id, values)=>{
  let result = await Product.updateOne({_id:id}, values);
  return result;	
};

module.exports = Product;
