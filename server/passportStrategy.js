const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const User = require("./models/user");
const bcrypt = require("bcrypt");

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user){
    if(err){
      done(err);
    } else {done(null, user);}
  });
});

passport.use(new LocalStrategy({
  usernameField:"email",
  passwordField:"password"
}, function(username, password, done) {
    User.findOne({ email:username }, async function(err, user) {
      if (err) return done(err);
      if (!user) return done(null, false, { message:"Incorrect username." });
      const match = await bcrypt.compare(password, user.password);
      if (!match) return done(null, false, { message:"Incorrect password." });
      return done(null, user);
    });
  } 
));



