const wrap = require("./wrap");
const express = require("express");
const router = express.Router();
const PreOrder = require("../models/preOrder");
const ProductInCart = require("../models/productInCart");

const addNew = wrap(async (req, res)=>{
  res.send(await PreOrder.addNew(req.body));
});

const getLastOne = wrap(async (req, res)=>{
  res.send(await PreOrder.getLastOne(req.session.user.id));
});

const removeAll = wrap(async (req, res, next)=>{
  await PreOrder.removeAll(req.session.user.id);
  next();
});

const removeProdInCart = wrap(async (req, res)=>{
  res.send(await ProductInCart.removeAll(req.session.user.id));
});

router.post("/addPreOrder", addNew);
router.get("/getLastOne", getLastOne);
router.delete("/delPreAndCartOrders", removeAll, removeProdInCart);

module.exports = router;
