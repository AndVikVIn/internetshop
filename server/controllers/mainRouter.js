const express = require("express");
const router = express.Router();

router.use(require("./productRouter"));

router.use(require("./userRoter"));

router.use(require("./productInCartRouter"));

router.use(require("./typeRouter"));

router.use(require("./orderRouter"));

router.use(require("./preOrderRouter"));


module.exports = router;
