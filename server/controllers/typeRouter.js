const wrap = require("./wrap");
const express = require("express");
const router = express.Router();
const Type = require("../models/type");

const showTree = wrap(async(req, res)=>{
  res.send(await Type.showTree());
});

router.get("/typeTree", showTree);

module.exports = router;
