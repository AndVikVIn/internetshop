const wrap = require("./wrap");
const express = require("express");
const router = express.Router();
const Product = require("../models/product");
const upload = require("../middleware/pictureUploader");

const returnPath =(req, res)=>{
  const path =req.file.path.substring(6);
  res.send({path});
};

const createNew = wrap(async (req, res)=>{
  res.send(await Product.createNew(req.body));
});

const getAll = wrap(async (req, res)=>{
  res.send(await Product.getAll());
});

const increaseRating = wrap(async (req, res)=>{
  res.send(await Product.increaseRating(req.body.id, req.body));
});

router.post("/newProduct", createNew);
router.get("/products", getAll);
router.post("/upload", upload.single("productPicture"), returnPath);
router.post("/products", increaseRating);

module.exports = router;

