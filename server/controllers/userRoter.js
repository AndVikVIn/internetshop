const wrap = require("./wrap");
const express = require("express");
const router = express.Router();
const User = require("../models/user");
const passport = require("passport");
require("../passportStrategy");
const adminCheck = require("../middleware/adminCheck");

const createNew = wrap(async (req, res)=>{
  res.send(await User.createNew(req.body));
});

const getAll = wrap(async (req, res)=>{
  let result = await User.getAll();
  res.send(result);
});

const updateUser = wrap(async (req, res)=>{
  let result = await User.updateUser(req.body.id, req.body.property, req.body.value);
  res.send(result);
});

const checkEmail = wrap(async (req, res)=>{
  res.send(await User.checkEmail(req.query.email));
});

const logout = wrap(async (req, res)=>{
  req.session.destroy(()=>{
    res.send({});
  });
});

const sendStatus = wrap(async (req, res)=>{
  res.send(req.session.user || null);
});


const login =wrap(async (req, res, next)=>{
  passport.authenticate("local", (err, user)=>{
    if (err) return next(err);
    if (!user) res.send({});
    req.logIn(user, (err)=>{
      if (err) return next(err);
      req.session.user = user;
      res.send(user);
    });
  })(req, res, next);
});

router.get("/showUsers", adminCheck, getAll);
router.post("/updateUser", adminCheck, updateUser);
router.post("/login", login);
router.post("/logout", logout);
router.post("/login/status", sendStatus);
router.post("/newUser", createNew);
router.get("/checkEmail", checkEmail);

module.exports = router;
