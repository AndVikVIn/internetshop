const wrap = require("./wrap");
const express = require("express");
const router = express.Router();
const Order = require("../models/order");
const adminCheck = require("../middleware/adminCheck");


const addNew = wrap(async (req, res)=>{
  let result = await Order.addNew(req.body);
  res.send(result);
});

const changeOne = wrap(async (req, res)=>{
  res.send(await Order.changeOne(req.body));
});

const getAll = wrap(async (req, res)=>{
  res.send(await Order.getAll());
});

const getUserOrders = wrap(async (req, res)=>{
  res.send(await Order.getUserOrders(req.session.user.id));
});


router.get("/getOrders", adminCheck, getAll);
router.post("/addOrder", addNew);
router.get("/getHistory", getUserOrders);
router.post("/changeOrder", adminCheck, changeOne);

module.exports = router;
