const wrap = require("./wrap");
const express = require("express");
const router = express.Router();
const ProductInCart = require("../models/productInCart");

const addNew = wrap(async (req, res)=>{
  res.send(await ProductInCart.addNew(req.body));
});

const countAmount = wrap(async (req, res)=>{
  let result = await ProductInCart.countAmount(req.session.user.id);
  res.send({"allAdded":result});
});

const getAll = wrap(async (req, res)=>{
  res.send(await ProductInCart.getAll(req.session.user.id));
});

const removeOne = wrap(async (req, res)=>{
  res.send(await ProductInCart.removeOne(req.body.id));
});

router.post("/addToCart", addNew);
router.get("/countAmount", countAmount);
router.get("/productsInCart", getAll);
router.delete("/productsInCart", removeOne);

module.exports = router;

