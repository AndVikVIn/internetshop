const mongoose = require("mongoose");

const productTypeSchema = mongoose.Schema({
  id:mongoose.Schema.Types.ObjectId,
  value:String,
});

module.exports = productTypeSchema;
