const mongoose = require("mongoose");

const productInCartScheme = mongoose.Schema({
  id:String,
  manufacter:{
    type:mongoose.Schema.Types.ObjectId,
    ref:"manufacters"
  },
  type:{
    type:mongoose.Schema.Types.ObjectId,
    ref:"types"
  },
  name:String,
  sum:Number,
  price:Number,
  picture:String,
  amount:Number,
  userId:{
    type:mongoose.Schema.Types.ObjectId,
    ref:"users"
  }
});

module.exports = productInCartScheme;
