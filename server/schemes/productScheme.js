const mongoose = require("mongoose");

const productSchema = mongoose.Schema({
  id:String,
  manufacter:{
    type:mongoose.Schema.Types.ObjectId,
    ref:"manufacters"
  },
  type:{
    type:mongoose.Schema.Types.ObjectId,
    ref:"types"
  },
  name:String,
  price:Number,
  rating:Number,
  picture:String
});

module.exports = productSchema;
