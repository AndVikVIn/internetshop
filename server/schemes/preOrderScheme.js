const mongoose = require("mongoose");

const preOrderSchema = mongoose.Schema({
  id:String,
  name:String,
  amount:String,
  orderDate:Date,
  userId:String
});

module.exports = preOrderSchema;
