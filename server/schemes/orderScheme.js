const mongoose = require("mongoose");

const orderSchema = mongoose.Schema({
  id:String,
  amount:Array,
  name:Array,
  bayerName:String,
  bayerEmail:String,
  bayerPhone:String,
  address:String,
  orderDate:Date,
  paymentType:String,
  deliveryType:String,
  declineReason:String,
  status:String,
  userId:String
});

module.exports = orderSchema;
