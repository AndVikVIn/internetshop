const adminCheck = (req, res, next)=>{
  if(req.session.user.admin){
    next();
  } else {
    res.end();
  }
};

module.exports = adminCheck;
