import {JetView} from "webix-jet";
import DeclineWin from "./declineWin";

class UserHistoryPage extends JetView{
  config(){

    const historyTable = {
      view:"datatable",
      localId:"historyTable",
      url:"/api/getHistory",
      fixedRowHeight:false,
      rowLineHeight:25,
      rowHeight:80,
      columns:[
        {id:"name", header:["Product", {content:"textFilter"}], sort:"string", fillspace:true},
        {id:"amount", header:"Amount"},
        {id:"address", header:"Address", fillspace:true},
        {id:"deliveryType", header:"Delivery", adjust:"data"},
        {id:"paymentType", header:"Payment"},
        {id:"orderDate", header:"Order date", adjust:"data"},
        {id:"status", header:"Status"}
      ],
      on:{
        onItemDblClick:(obj)=>{
          const item = this.$$("historyTable").getItem(obj.toString());
          if(item.status == "Declined"){
            this.declineWin.showWin(item.declineReason);
          }
        }
      }
    };
    return historyTable;
  }
  init(){
    this.declineWin = this.ui(DeclineWin);
  }
}

export default UserHistoryPage;
