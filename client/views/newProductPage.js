import {JetView} from "webix-jet";

class NewProductPage extends JetView{
  config(){
    
    const addPicture = {
			view:"uploader", 
      value:"Add picture",
      inputName:"productPicture",
      localId:"pictureUploader",
      link:"mylist",
			inputWidth:250,
      align:"right",
      autosend:false, 
      upload:"/api/upload",
    };

    const productForm = {
      view:"form",
      localId:"newProdForm",
      elementsConfig:{
        labelWidth:150,
        labelAlign:"left"
      },
      elements:[
        {rows:[
          {view:"text", name:"name", required:true, label:"Name"},
          {view:"text", name:"price", required:true, label:"Price"},
          {cols:[
            {template:"Picture:", borderless:true},
            {rows:[
              { view:"list",  id:"mylist",  type:"uploader", autoheight:true, borderless:true },
              addPicture
            ]}
          ]},
          {
            view:"textarea",
            name:"details",
            height:250,
          },
          {view:"button", value:"Add new product", click:()=>{
            const values = this.$$("newProdForm").getValues();
            this.$$("pictureUploader").send((obj)=>{
              values.picture = obj.path;
              webix.ajax().post("/api/newProduct", values);
            });
          }}
        ]}
         
      ]
    };
    
    return productForm;
  }
}

export default NewProductPage;
