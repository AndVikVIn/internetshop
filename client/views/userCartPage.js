import {JetView} from "webix-jet";
import "../styles/app.css";

class UserCartPage extends JetView{
  config(){
    const datatable = {
      view:"datatable",
      localId:"userCartTable",
      url:"/api/productsInCart",
      rowHeight:120,
      footer:true,
      columns:[
        {id:"post", header:"Poster", footer:{text:"Total:"}, width:200, template:function(obj){
          if(obj.picture == undefined){
           return "<img style='width:100px;height:110px' src='/img/default.jpeg'>";
          }
           return "<img style='width:100px;height:110px' src='" + obj.picture +"'>";
         }},
        {id:"name", header:"Name", fillspace:true},
        {id:"amount", header:"Amount"},
        {id:"price", header:"Price"},
				{id:"sum", header:"Sum", footer:{ content:"summColumn" }},
        {id:"trash", header:"", template:"{common.trashIcon()}"}    
      ],
      onClick:{
        "wxi-trash":(obj, id)=>{
          const item = this.$$("userCartTable").getItem(id);
          this.$$("userCartTable").remove(id);
          webix.ajax().del("/api/productsInCart", item).then(()=>{
            return webix.ajax().get("/api/countAmount");
          })
          .then((data)=>{
            let total = data.json().allAdded;
            this.app.callEvent("changeBadge", [total]);
          })
          .catch(()=>{
            webix.message("Server error occured. Please try again later.", "error");
          });
        }
      },
      on:{
        onAfterRender:()=>{
          webix.ajax().get("/api/countAmount").then((data)=>{
            let total = data.json().allAdded;
            this.app.callEvent("changeBadge", [total]);
          }).catch(()=>{
            webix.message("Server error occured. Please try again later.", "error");
          });
        }
      }
    };
    const makeOrderBut = {
      view:"button",
      value:"Make order",
      inputWidth:250,
      click:()=>{
        const user = this.app.getService("user").getUser();
        const orderDate = new Date();
        const items = this.$$("userCartTable").serialize();
        const order = {};
        order.id = items[0]._id;
        order.orderDate = orderDate;
        order.amount = [];
        order.name = [];
        order.userId = user.id;
        const length = items.length;
        for(let i = 0; i < length; i++){
          order.amount.push(items[i].amount);
          order.name.push(items[i].name);
        }
        order.amount = order.amount.join();
        order.name = order.name.join();
        webix.ajax().post("/api/addPreOrder", order).then(()=>{
          this.app.callEvent("proceedToCheckout");
        }).catch(()=>{
          webix.message("Server error occured. Please try again later.", "error");
        });          
      }
    };

    return {
      rows:[
        datatable,
        makeOrderBut
      ]
    };
  }
}

export default UserCartPage;
