import {JetView} from "webix-jet";

class ClientInfo extends JetView{
  config(){
    
    let userNumber = 0;
    const clientTable = {
      view:"datatable",
      localId:"clientTable",
      url:"/api/showUsers",
      scheme:{
        $init:(obj)=>{
          obj.number = ++userNumber; 
        }
      },
      editable:true,
      editaction:"dblclick",
      columns:[
        {id:"number", header:"#"},
        {id:"name", header:["Name", {content:"textFilter"}], editor:"text", fillspace:true},
        {id:"email", localId:"email", header:["Email", {content:"textFilter"}], editor:"text", fillspace:true},
        {id:"creationDate", header:"Created at", fillspace:true},
      ],
      on:{
        onAfterEditStop:function(values, item){
          const editedUser = this.getItem(item.row);
          if(this.validate()){
            webix.ajax().post("/api/updateUser", {id:editedUser._id, property:item.column, value:values.value})
            .catch(()=>{
              webix.message("Server error occured. Please try again later.", "error");
            });
          }
        }
      },
      rules:{
        "name":webix.rules.isNotEmpty,
        "email":webix.rules.isEmail,
      }
    };
    return clientTable;
  }
}

export default ClientInfo;
