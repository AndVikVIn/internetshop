import {JetView} from "webix-jet";

class DeclineWin extends JetView{
  config(){

    return{
      view:"window",
      type:"clean",
      localId:"declineWin",
      head:{
        view:"template", 
        template:"<span>Decline reasons</span><span class='webix_icon wxi-close detInfoClose'></span>",
        onClick:{
          "wxi-close":function(){
            this.hide();
          }
        },
      },
      position:"center", height:400, width:400,
      body:{
        template:"Reason"
      },
      onClick:{
        "wxi-close":()=>{
          this.getRoot().hide();
        }
      },
    };
  }
  showWin(reason){
    webix.ui({
        template:reason
    }, this.$$("declineWin"), this.$$("declineWin"));
    this.getRoot().show();
  }
  hideWin(){
    this.getRoot().hide();
  }
}

export default DeclineWin;
