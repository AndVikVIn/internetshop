import {JetView} from "webix-jet";
import StatusWin from "./statusWin";

class OrdersPage extends JetView{
  config(){

    let ordersNumber = 0;
    const ordersTable = {
      view:"datatable",
      localId:"ordersTable",
      url:"/api/getOrders",
      save:"->/api/changeOrder",
      scheme:{
        $init:(obj)=>{
          obj.number = ++ordersNumber; 
        }
      },
      fixedRowHeight:false,
      rowLineHeight:25,
      rowHeight:80,
      columns:[
        {id:"number", header:"#"},
        {id:"name", header:["Product", {content:"textFilter"}], sort:"string", fillspace:true},
        {id:"amount", header:"Amount"},
        {id:"bayerName", header:["Bayer name", {content:"textFilter"}]},
        {id:"bayerPhone", header:"Phone", fillspace:true},
        {id:"bayerEmail", header:"E-mail"},
        {id:"address", header:"Address"},
        {id:"deliveryType", header:"Delivery"},
        {id:"paymentType", header:"Payment"},
        {id:"orderDate", header:"Order date", fillspace:true},
        {id:"status", header:"Status"}
      ],
      on:{
        onItemDblClick:(obj)=>{
          const item = this.$$("ordersTable").getItem(obj.row);
          if(obj.column == "status"){
            this.statusWin.showWin(item);
          }
        }
      }
    };
    return ordersTable;
  }
  init(){
    this.statusWin = this.ui(StatusWin);
    this.on(this.app, "refreshOrdersTable", (id, values)=>{
      this.$$("ordersTable").updateItem(id, values);
    });
  }
}

export default OrdersPage;
