import {JetView} from "webix-jet";
import "../styles/app.css";


class adminPage extends JetView{
  config(){
    const user = this.app.getService("user").getUser();

    const toolbar = {
      view:"toolbar",
      localId:"toolbar",
      cols:[
        {view:"label", label:"Andrey's shop", css:"headerLabel", click:()=>{
          this.show("/adminPage/clientInfo");
        }},
        {view:"label", label:"Hello" + " " + user.name, css:"headerLabel"},
        {view:"button", value:"Logout", width:100, click:()=>{
          this.app.getService("user").logout().then(()=>{this.app.show("afterLogout");});
        }},
        {view:"button", value:"History", width:100, click:()=>{
          this.show("/adminPage/userHistoryPage");
        }},
        {view:"button", localId:"cartBadge", value:"Cart", badge:0, width:100, click:()=>{
          this.show("/adminPage/userCartPage");
        }}
      ]
    };
    
    const adminPanel = {
      view:"list",
      localId:"adminPanel",
      select:true,
      width:200,
      template:"#value#",
      data:[
        {id:"clientInfo", value:"Client info"},
        {id:"ordersPage", value:"Orders"},
        {id:"newProductPage", value:"Add new product"},
      ],
      on:{
        onAfterSelect:(id)=>{
         this.app.show("adminPage/"+id);
        }
      }
    };

    return {
      rows:[
        toolbar,
        {cols:[
          adminPanel,
          {$subview:true}
        ]}
      ]
    };
  }
}

export default adminPage;
