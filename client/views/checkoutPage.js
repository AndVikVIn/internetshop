import {JetView} from "webix-jet";

class CheckoutPage extends JetView {
  config(){

    const checkForm = {
      view:"form",
      localId:"checkForm",
      elementsConfig:{
        labelWidth:150,
        labelAlign:"left"
      },
      elements:[
        {view:"text", label:"Your Name", name:"bayerName", required:true, placeholder:"Type your name", invalidMessage:"Name can't be empty"},
        {view:"text", label:"E-mail", name:"bayerEmail", required:true, placeholder:"Type your e-mail", invalidMessage:"Incorrect e-mail"},
        {view:"text", label:"Phone", name:"bayerPhone", required:true,  pattern:{ mask:"###-## #######", allow:/[0-9]/g}, placeholder:"375-## #######", invalidMessage:"Incorrect phone number"},
        {view:"select", label:"Delivery type", name:"deliveryType", value:1, options:[
          {id:"Economy delivery", value:"Economy delivery"},
          {id:"Fast delivery", value:"Fast delivery"},
          {id:"Self pickup", value:"Self pickup"}
          ]
        },
        {view:"text", label:"Delivery address", name:"address", required:true, placeholder:"Type your address", invalidMessage:"Address can't be empty"},
        {view:"select", label:"Payment type", name:"paymentType", options:[
          {id:"Card", value:"Card"},
          {id:"Cash", value:"Cash"},
          {id:"Web money", value:"Web money"}
          ]
        },
      ],
      rules:{
        bayerName:webix.rules.isNotEmpty,
        bayerEmail:webix.rules.isEmail,
        bayerPhone:(obj)=>{
          if(obj.length !== 12){
            return false;
          }
          return true;
        },
        address:webix.rules.isNotEmpty
      }
    };

    const checkButton = {
      view:"button",
      value:"Checkout",
      click:()=>{
        const values = this.$$("checkForm").getValues();
        if(this.$$("checkForm").validate()){
          webix.ajax().get("/api/getLastOne").then((data)=>{
            const preOrder = data.json();
            const order = {...preOrder, ...values};
            order.status = "In progress";
            return webix.ajax().post("/api/addOrder", order);
          })
          .then(()=>{
            return webix.ajax().del("api/delPreAndCartOrders");
          })
          .then(()=>{
            this.app.callEvent("changeBadge", [0]);
            webix.message("Thank you for your order!", "info");
            this.app.callEvent("showHistory");
          })
          .catch(()=>{
            webix.message("Server error occured. Please try again later.", "error");
          });
        }
      }
    };
    return{
      rows:[
        checkForm,
        checkButton
      ]
    };
  }
}

export default CheckoutPage;
