import {JetView} from "webix-jet";

class StatusWin extends JetView{
  config(){
    
    return{
      view:"window",
      type:"clean",
      localId:"statusWin",
      head:{
        view:"template",
        template:"<span>Change status</span><span class='webix_icon wxi-close detInfoClose'></span>",
        onClick:{
          "wxi-close":function(){
            this.hide();
          }
        }
      },
      position:"center", height:400, width:400,
      body:{
        view:"form",
        localId:"statusForm",
        rows:[
          {
            view:"select",
            label:"Choose status",
            name:"status",
            labelWidth:150,
            labelAlign:"left",
            options:[
              {id:"In progress", value:"In progress"},
              {id:"Finished", value:"Finished"},
              {id:"Declined", value:"Declined"}
            ],
            on:{
              onChange:(obj)=>{
                if(obj == "Declined"){
                  this.$$("reasonText").show();
                } else{
                  const item = this.$$("statusForm").getValues();
                  delete item.reason;
                  this.$$("reasonText").hide();
                }
              }
            }
          },
          {
            view:"textarea",
            localId:"reasonText",
            name:"reason",
            label:"Indicate reason",
            labelWidth:150,
            labelAlign:"left",
            hidden:true
          },
          {
            view:"button",
            value:"Save",
            click:()=>{
              const item = this.$$("statusForm").getValues();
              this.app.callEvent("refreshOrdersTable", [item.id, item]);
              this.getRoot().hide();
            }
          } 
        ]
      },
      onClick:{
        "wxi-close":()=>{
          this.getRoot().hide();
        }
      }
    };
  }
  showWin(item){
    this.$$("statusForm").setValues(item);
    this.getRoot().show();
  }
}

export default StatusWin;
