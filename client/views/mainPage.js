import {JetView} from "webix-jet";
import "../styles/app.css";


class mainPage extends JetView{
  config(){
    const user = this.app.getService("user").getUser();
    const toolbar = {
      view:"toolbar",
      localId:"toolbar",
      cols:[
        {view:"label", label:"Andrey's shop", css:"headerLabel", click:()=>{
          this.show("/mainPage/userMainPage");
        }},
        {view:"label", label:"Hello" + " " + user.name, css:"headerLabel"},
        {view:"button", value:"Logout", width:100, click:()=>{
          this.app.getService("user").logout().then(()=>{this.app.show("afterLogout");});
        }},
        {view:"button", value:"History", width:100, click:()=>{
          this.show("/mainPage/userHistoryPage");
        }},
        {view:"button", localId:"cartBadge", value:"Cart", badge:0, width:100, click:()=>{
          this.show("/mainPage/userCartPage");
        }}
      ]
    };
    
    const treetable = {
      view:"tree",
      localId:"productsTreeTable",
      activeTitle:true,
      select:true,
      width:200,
      template:"{common.icon()}" + "<span>#value#</span>",
      url:"/api/typeTree",
      on:{
        onAfterSelect:function(id){
          const currentItem = this.getSelectedItem();
          const firstId = this.getFirstId();
          this.$scope.app.callEvent("filterByTree", [id, firstId, currentItem]);
        }
      }
    };

    return {
      rows:[
        toolbar,
        {cols:[
          treetable,
          {$subview:true}
        ]}
      ]
    };
  }
  init(){
    this.on(this.app, "changeBadge", (count, item)=>{
        this.$$("cartBadge").define("badge", count);
        this.$$("cartBadge").refresh();
        if(item){         
          webix.message( `${item.amount} X ${item.name} added to cart`, "info");
        }
    });
    this.on(this.app, "proceedToCheckout", ()=>{
      this.show("/mainPage/checkoutPage");
    });
    this.on(this.app, "showHistory", ()=>{
      this.show("/mainPage/userHistoryPage");
    });
  }
}

export default mainPage;
