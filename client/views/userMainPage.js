import {JetView} from "webix-jet";
import "../styles/app.css";
import DetInfo from "./detInfoPopup";

webix.protoUI({
  name:"productTable"
}, webix.ui.datatable, webix.ActiveContent);

class UserMainPage extends JetView{
  config(){
    const user = this.app.getService("user").getUser();
    const datatable = {
      view:"productTable",
      localId:"productsTable",
      activeContent:{
        amount:{
          view:"counter",
          width:120,
          height:40,
        }
      },
      url:"/api/products",
      save:"->/api/products",
      rowHeight:120,
      columns:[
        {id:"post", header:"Poster", width:200, template:function(obj){
          if(obj.picture == undefined){
           return "<img style='width:100px;height:110px' src='/img/default.jpeg'>";
          }
           return "<img style='width:100px;height:110px' src='" + obj.picture +"'>";
         }},
				{id:"name", header:["Name", {content:"textFilter"}], sort:"string", fillspace:true},
				{id:"price", header:"Price", sort:"int"},
        {id:"rating", header:"Rating", sort:"int"},
        {id:"amount", header:"Amount", width:150, template:"<div>{common.amount()}</div>"},
        {id:"cart", header:"Cart", template:"<i class='fas fa-shopping-cart fa-2x'></i>"}    
      ],
      onClick:{
        "fa-shopping-cart":(obj, id)=>{
          const item = this.$$("productsTable").getItem(id);
          if(item.amount){
            item.userId = user.id;
            item.sum = item.amount * item.price;
            webix.ajax().post("/api/addToCart", item).then(()=>{
              return webix.ajax().get("/api/countAmount");
            })
            .then((data)=>{
              let total = data.json().allAdded;
              this.app.callEvent("changeBadge", [total, item]);
            })
            .catch(()=>{
                webix.message("Server error occured. Please try again later.", "error");
            }); 
          }
        }
      },
      on:{
        onItemDblClick:(obj)=>{
          if(obj.column == "name" || obj.column == "post"){
                 const item = this.$$("productsTable").getItem(obj.row);
          this.detInfoPopup.showPopup(item);
          }
        },
        onAfterRender:()=>{
          webix.ajax().get("/api/countAmount").then((data)=>{
            let total = data.json().allAdded;
            this.app.callEvent("changeBadge", [total]);
          }).catch(()=>{
            webix.message("Server error occured. Please try again later.", "error");
          });
        }
      }
    };
    return datatable;
  }
  init(){
    this.detInfoPopup = this.ui(DetInfo);
    this.on(this.app, "increaseRating", (id, values)=>{
      this.$$("productsTable").updateItem(id, values);
    });
    this.on(this.app, "filterByTree", (id, firstId, currentItem)=>{
      if(id != firstId){
        this.$$("productsTable").filter((obj)=>{
          if(obj.manufacter == currentItem._id){
            return obj;
          }
        });
      } else this.$$("productsTable").filter();
    });
  }
}

export default UserMainPage;
