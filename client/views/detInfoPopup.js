import {JetView} from "webix-jet";

class DetInfo extends JetView {
  config(){
    return{
      view:"window",
      type:"clean",
      head:{
        view:"template",
        localId:"header", 
        template:"<span>#name#</span><span class='webix_icon wxi-close detInfoClose'></span>",
        onClick:{
          "wxi-close":()=>{
            this.getRoot().hide();
          }
        }
      },
      position:"center", height:350, width:600,
      body:{
        localId:"detInfo",
        borderless:true,
        rows:[
          {cols:[
            {localId:"image", type:"clean", width:230, css:"detInfoImage", template:function(obj){
              if(obj.picture == undefined){
              return "<img src='/img/default.jpeg'>";
              }
              return "<img src='" + obj.picture +"'>";
              }
          },
            {
              localId:"info", type:"clean", 
              template:"<div class='detInfoFields'><strong>Name</strong>: #name#</div><div class='detInfoFields'><strong>Price</strong>: #price#</div><div class='detInfoFields'><strong>Rating</strong>: #rating#<span class='far fa-star'></span></div>",
              onClick:{
                "fa-star":()=>{
                  const item = this.$$("info").data;
                  item.rating++;
                  this.app.callEvent("increaseRating", [item.id, item]);
                  this.$$("info").refresh();
                }
              }
            },
          ]}
        ] 
      }
    };
  }
  showPopup(item){
    this.$$("header").parse(item);
    this.$$("image").parse(item);
    this.$$("info").parse(item);
    this.getRoot().show();
  }
}

export default DetInfo;
