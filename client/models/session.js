function status(){
	return webix.ajax().post("/api/login/status")
		.then(data => data.json());
}

function login(data){
  return webix.ajax().post("/api/login", data
  ).then(data => data.json());
}

function logout(){
	return webix.ajax().post("/api/logout")
		.then(data => data.json());
}

export default {
	status, login, logout
};
