import "./styles/app.css";
import {JetApp, EmptyRouter, HashRouter, plugins} from "webix-jet";
import session from "./models/session";

export default class MyApp extends JetApp{
  constructor(config){
    const defaults = {
      id:APPNAME,
      version:VERSION,
      router:BUILD_AS_MODULE ? EmptyRouter : HashRouter,
			debug:!PRODUCTION,
			start:"/loginPage"
    };
    
    super({ ...defaults, ...config });
    this.use( plugins.User, { 
      model:session,
      login:"/loginPage",
      afterLogout:"/loginPage",
    });
	}
}
if (!BUILD_AS_MODULE){
  webix.ready(() => {
    const app =	new MyApp();
    app.attachEvent("app:guard", (url, view, nav)=>{
      if (url.indexOf("/adminPage") == 0){
        if(!app.config.access){
          nav.redirect = "mainPage/userMainPage";
        }
      }
    });
    app.render();
  });
}
